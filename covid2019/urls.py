"""covid2019 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import url
from patient import views, pdfs

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^api/patients$', views.patients_list, name="patients_list"),
    url(r'^api/patients(?P<name>[\w]+)$', views.patients_list),
    url(r'^api/patients/dead$', views.patients_dead, name="patients_dead"),
    url(r'^api/patients/recovered/$', views.patients_recovered, name="patients_recovered"),
    url(r'^api/patients/confirmed/$', views.patients_confirmed, name="patients_confirmed"),
    url(r'^api/patients/suspect/$', views.patients_suspect, name="patients_suspect"),
    url(r'^api/patients/(?P<pk>[0-9]+)$', views.patient_by_id, name="patient_by_id"),

    # Para los pdfs
    path('pdf/recovered/<int:dia1>/<int:mes1>/<int:anio1>/<int:dia2>/<int:mes2>/<int:anio2>', pdfs.recuperados),
    path('pdf/confirmed/<int:dia1>/<int:mes1>/<int:anio1>/<int:dia2>/<int:mes2>/<int:anio2>', pdfs.confirmados),
    path('pdf/suspected/<int:dia1>/<int:mes1>/<int:anio1>/<int:dia2>/<int:mes2>/<int:anio2>', pdfs.sospechosos),
    path('pdf/statistics/<int:dia1>/<int:mes1>/<int:anio1>/<int:dia2>/<int:mes2>/<int:anio2>', pdfs.statistics)
]
