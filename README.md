# calidad-pacientes-covid

## Pasos para instalar Django

* Instalar y activar un [entorno virtual](https://www.programaenpython.com/miscelanea/crear-entornos-virtuales-en-python/)
* Instalar las dependencias `pip install django djangorestframework django-cors-headers`

## Para correr Django

* En la ruta del proyecto `python manage.py runserver`
* Abrir localhost:8000

## Para ingresar al admin
* ir a localhost:8000/admin
* user: admin  
password: admin
