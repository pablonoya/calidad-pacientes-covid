from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework import status

from .models import Patient
from .serializers import PatientSerializer


@api_view(['GET', 'POST'])
def patients_list(request):
    """List patients or create a new patient"""
    name = request.GET.get("name")

    if request.method == 'GET':
        patients = Patient.objects.all()

        if name:
            patients = patients.filter(first_name__contains=name)

        # convierte la lista de pacientes a json
        serializer = PatientSerializer(
            patients, context={'request': request}, many=True)
        return Response({'data': serializer.data},
                        status=status.HTTP_200_OK)

    elif request.method == 'POST':
        serializer = PatientSerializer(data=request.data)
        if serializer.is_valid():
            # guarda el paciente
            serializer.save()
            return Response(serializer.data,
                            status=status.HTTP_201_CREATED)
        return Response(serializer.errors,
                        status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def patients_confirmed(request):
    """List patients by state"""

    if request.method == 'GET':
        # traemos los pacientes recuperados C=confirmado
        patients = Patient.objects.filter(state__startswith="C")

        # convierte la lista de pacientes a json
        serializer = PatientSerializer(
            patients, context={'request': request}, many=True)
        return Response({'data': serializer.data},
                        status=status.HTTP_200_OK)


@api_view(['GET'])
def patients_suspect(request):
    """List patients by state"""

    if request.method == 'GET':
        # traemos los pacientes recuperados S=sospechoso
        patients = Patient.objects.filter(state__startswith="S")

        # convierte la lista de pacientes a json
        serializer = PatientSerializer(
            patients, context={'request': request}, many=True)
        return Response({'data': serializer.data},
                        status=status.HTTP_200_OK)


@api_view(['GET'])
def patients_recovered(request):
    """List patients by state"""

    if request.method == 'GET':
        # traemos los pacientes recuperados R=recuperado
        patients = Patient.objects.filter(state__startswith="R")

        # convierte la lista de pacientes a json
        serializer = PatientSerializer(
            patients, context={'request': request}, many=True)
        return Response({'data': serializer.data},
                        status=status.HTTP_200_OK)


@api_view(['GET'])
def patients_dead(request):
    """List patients by severity"""

    if request.method == 'GET':
        # traemos los pacientes fallecidos FC=fallecido
        patients = Patient.objects.filter(severity__startswith="FC")

        # convierte la lista de pacientes a json
        serializer = PatientSerializer(
            patients, context={'request': request}, many=True)
        return Response({'data': serializer.data},
                        status=status.HTTP_200_OK)


@api_view(['GET', 'PUT'])
def patient_by_id(request, pk):
    """Get or update a patient by pk"""
    # Por si no existe ese paciente
    try:
        patient = Patient.objects.get(pk=pk)
    except Patient.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        # Traemos el paciente
        serializer = PatientSerializer(patient, context={'request': request})
        return Response(serializer.data, status=status.HTTP_200_OK)

    elif request.method == 'PUT':
        # Actualizamos el estado paciente
        serializer = PatientSerializer(
            patient, data=request.data, context={"request": request})

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
