from django.urls import reverse
from rest_framework.test import APITestCase
from rest_framework import status
from .models import Patient


class PatientTests(APITestCase):
    def setUp(self):
        self.url = reverse("patients_list")
        self.data = {"first_name": "Paciente",
                     "last_name": "Prueba",
                     "age": 30,
                     "sex": "M",
                     "state": "S",
                     "severity": "LV"}

        self.client.post(self.url, self.data, format="json")

    def test_new_patient_missing_info(self):
        data = {"first_name": "Juan",
                "sex": "M"}
        response = self.client.post(self.url, data, format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_new_patient_ok_info(self):
        data = {"first_name": "Juan",
                "last_name": "Pérez",
                "age": 30,
                "sex": "M"}
        response = self.client.post(self.url, data, format="json")

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_get_patients_list(self):
        response = self.client.get(self.url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['data']), 1)

    def test_get_patients_by_name(self):
        url = self.url + "?name=Paciente"
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEquals(response.data['data'][0]["first_name"], "Paciente")

    def test_get_dead_patients(self):
        url = reverse("patients_dead")
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotEquals(len(response.data['data']), 1)

    def test_get_suspect_patients(self):
        url = reverse("patients_suspect")
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIs(Patient.objects.get().state, "S")

    def test_get_confirmed_patients(self):
        url = reverse("patients_confirmed")
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['data']), 0)

    def test_get_recovered_patients(self):
        url = reverse("patients_recovered")
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['data']), 0)

    def test_get_patient_by_id_wrong(self):
        url = reverse("patient_by_id", args=[10])
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_get_patient_by_id_ok(self):
        url = reverse("patient_by_id", args=[1])
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["last_name"], "Prueba")
        self.assertEqual(response.data["state"], "S")
