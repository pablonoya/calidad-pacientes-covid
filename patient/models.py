import datetime

from django.db import models

# Create your models here.
class Patient(models.Model):
    #  Genero. estado y gravedad del paciente
    SEX = (('F', 'Femenino'),  ('M', 'Masculino'))  
    STATE = (('S', 'Sospechoso'),  ('C', 'Confirmado'), ('R', 'Recuperado'))
    SEVERITY = (('LV', 'Leve'),  ('GV', 'Grave'), ('MG', 'Muy Grave'), ('FC', 'Fallecido')) 

    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    age = models.IntegerField()
    sex = models.CharField(max_length=1, choices=SEX)
    base_diseases = models.CharField(max_length=50, null=True, blank=True)
    state = models.CharField(max_length=1, choices=STATE, default='S')
    severity = models.CharField(max_length=2, choices=SEVERITY, default='LV')
    date_register = models.DateField(default=datetime.datetime.now)

    def __str__(self):
        string = "{0} {1}"
        return string.format(self.first_name, self.last_name)
