from django.contrib import admin

from .models import Patient

# Register your models here.


class PatientAdmin(admin.ModelAdmin):
    #  La tabla que se mostrara en el admin
    list_display = (
        'id', 'first_name', 'last_name', 'age', 'sex', 'base_diseases', 'state', 'severity', 'date_register'
    )

    #  Campos por los que se buscara
    search_fields = (
        'first_name', 'last_name', 'age', 'sex', 'base_diseases', 'state', 'severity', 'date_register'
    )

    #  Un filtro de busqueda
    list_filter = (
        'state', 'severity', 'sex'
    )


admin.site.register(Patient, PatientAdmin)
