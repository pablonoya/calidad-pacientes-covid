import datetime

from io import BytesIO
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4, inch
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.platypus import Paragraph, Table, TableStyle
from reportlab.lib.enums import TA_CENTER
from reportlab.lib import colors

from django.http import HttpResponse

from .models import Patient


def sospechosos(request, dia1, mes1, anio1, dia2, mes2, anio2):
    """Generación del pdf para los pacientes sospechosos"""
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename=sospechosos.pdf'

    buffer = BytesIO()
    c = canvas.Canvas(buffer, pagesize=A4)

    # La cabecera
    c.setLineWidth(.3)
    c.setFont('Helvetica', 18)
    c.drawString(30, 750, 'Reporte de Sospechosos')

    c.setFont('Helvetica', 10)
    c.drawString(460, 750, "del {}-{}-{} al {}-{}-{}".format(dia1, mes1, anio1, dia2, mes2, anio2))

    # traemos los pacientes recuperados R=recuperado
    start = datetime.datetime(int(anio1), int(mes1), int(dia1))
    end = datetime.datetime(int(anio2), int(mes2), int(dia2))

    patients = Patient.objects.filter(state__startswith="S", date_register__range=[start, end])

    # La tabla
    styles = getSampleStyleSheet()
    styleBH = styles['Normal']
    styleBH.alignment = TA_CENTER
    styleBH.fontSize = 10

    numero = Paragraph('''NRO''', styleBH)
    nombre = Paragraph('''NOMBRE''', styleBH)
    apellido = Paragraph('''APELLIDO''', styleBH)
    edad = Paragraph('''EDAD''', styleBH)
    sexo = Paragraph('''SEXO''', styleBH)
    enfermedades = Paragraph('''ENFERMEDAD BASE''', styleBH)
    fecha = Paragraph('''FECHA''', styleBH)

    data = []
    data.append([numero, nombre, apellido, edad, sexo, enfermedades, fecha])

    styleN = styles['BodyText']
    styleN.alignment = TA_CENTER
    styleN.fontSize = 7

    high = 650
    i = 1
    for p in patients:
        string = [i, p.first_name, p.last_name, p.age, p.sex, p.base_diseases, p.date_register]
        data.append(string)
        high = high - 18
        i += 1

    width, height = A4
    table = Table(data, colWidths=[
        0.5 * inch, 1 * inch, 1 * inch, 0.6 * inch, 0.6 * inch, 2 * inch, 1.5 * inch])
    table.setStyle(TableStyle([
        ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
        ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
    ]))

    table.wrapOn(c, width, height)
    table.drawOn(c, 30, high)
    c.showPage()

    # Guardamos todo
    c.save()
    pdf = buffer.getvalue()
    buffer.close()
    response.write(pdf)

    return response


def confirmados(request, dia1, mes1, anio1, dia2, mes2, anio2):
    """Generación del pdf para los pacientes confirmados"""
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename=confirmados.pdf'

    buffer = BytesIO()
    c = canvas.Canvas(buffer, pagesize=A4)

    # La cabecera
    c.setLineWidth(.3)
    c.setFont('Helvetica', 18)
    c.drawString(30, 750, 'Reporte de Confirmados')

    c.setFont('Helvetica', 10)
    c.drawString(460, 750, "del {}-{}-{} al {}-{}-{}".format(dia1, mes1, anio1, dia2, mes2, anio2))

    # traemos los pacientes recuperados C=Confirmado
    start = datetime.datetime(int(anio1), int(mes1), int(dia1))
    end = datetime.datetime(int(anio2), int(mes2), int(dia2))

    patients = Patient.objects.filter(state__startswith="C", date_register__range=[start, end])

    # La tabla
    styles = getSampleStyleSheet()
    styleBH = styles['Normal']
    styleBH.alignment = TA_CENTER
    styleBH.fontSize = 10

    numero = Paragraph('''NRO''', styleBH)
    nombre = Paragraph('''NOMBRE''', styleBH)
    apellido = Paragraph('''APELLIDO''', styleBH)
    edad = Paragraph('''EDAD''', styleBH)
    sexo = Paragraph('''SEXO''', styleBH)
    enfermedades = Paragraph('''ENFERMEDAD BASE''', styleBH)
    fecha = Paragraph('''FECHA''', styleBH)

    data = []
    data.append([numero, nombre, apellido, edad, sexo, enfermedades, fecha])

    styleN = styles['BodyText']
    styleN.alignment = TA_CENTER
    styleN.fontSize = 7

    high = 650
    i = 1
    for p in patients:
        string = [i, p.first_name, p.last_name, p.age, p.sex, p.base_diseases, p.date_register]
        data.append(string)
        high = high - 18
        i += 1

    width, height = A4
    table = Table(data, colWidths=[
        0.5 * inch, 1 * inch, 1 * inch, 0.6 * inch, 0.6 * inch, 2 * inch, 1.5 * inch])
    table.setStyle(TableStyle([
        ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
        ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
    ]))

    table.wrapOn(c, width, height)
    table.drawOn(c, 30, high)
    c.showPage()

    # Guardamos todo
    c.save()
    pdf = buffer.getvalue()
    buffer.close()
    response.write(pdf)

    return response


def recuperados(request, dia1, mes1, anio1, dia2, mes2, anio2):
    """Generación del pdf para los pacientes recuperados"""
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename=recuperados.pdf'

    buffer = BytesIO()
    c = canvas.Canvas(buffer, pagesize=A4)

    # La cabecera
    c.setLineWidth(.3)
    c.setFont('Helvetica', 18)
    c.drawString(30, 750, 'Reporte de Recuperados')

    c.setFont('Helvetica', 10)
    c.drawString(460, 750, "del {}-{}-{} al {}-{}-{}".format(dia1, mes1, anio1, dia2, mes2, anio2))

    # traemos los pacientes recuperados R = recuperado
    start = datetime.datetime(int(anio1), int(mes1), int(dia1))
    end = datetime.datetime(int(anio2), int(mes2), int(dia2))

    patients = Patient.objects.filter(state__startswith="R", date_register__range=[start, end])

    # La tabla
    styles = getSampleStyleSheet()
    styleBH = styles['Normal']
    styleBH.alignment = TA_CENTER
    styleBH.fontSize = 10

    numero = Paragraph('''NRO''', styleBH)
    nombre = Paragraph('''NOMBRE''', styleBH)
    apellido = Paragraph('''APELLIDO''', styleBH)
    edad = Paragraph('''EDAD''', styleBH)
    sexo = Paragraph('''SEXO''', styleBH)
    enfermedades = Paragraph('''ENFERMEDAD BASE''', styleBH)
    fecha = Paragraph('''FECHA''', styleBH)

    data = []
    data.append([numero, nombre, apellido, edad, sexo, enfermedades, fecha])

    styleN = styles['BodyText']
    styleN.alignment = TA_CENTER
    styleN.fontSize = 7

    high = 650
    i = 1
    for p in patients:
        string = [i, p.first_name, p.last_name, p.age, p.sex, p.base_diseases, p.date_register]
        data.append(string)
        high = high - 18
        i += 1

    width, height = A4
    table = Table(data, colWidths=[
        0.5 * inch, 1 * inch, 1 * inch, 0.6 * inch, 0.6 * inch, 2 * inch, 1.5 * inch])
    table.setStyle(TableStyle([
        ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
        ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
    ]))

    table.wrapOn(c, width, height)
    table.drawOn(c, 30, high)
    c.showPage()

    # Guardamos todo
    c.save()
    pdf = buffer.getvalue()
    buffer.close()
    response.write(pdf)

    return response


def statistics(request, dia1, mes1, anio1, dia2, mes2, anio2):
    """pdf de estadísticas"""
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename=estadísticas.pdf'

    buffer = BytesIO()
    c = canvas.Canvas(buffer, pagesize=A4)

    # La cabecera
    c.setLineWidth(.3)
    c.setFont('Helvetica', 18)
    c.drawString(30, 750, 'Reporte de estadísticas')

    c.setFont('Helvetica', 10)
    c.drawString(460, 750, f"del {dia1}-{mes1}-{anio1} al {dia2}-{mes2}-{anio2}")

    # La tabla
    styles = getSampleStyleSheet()
    style_header = styles['Normal']
    style_header.alignment = TA_CENTER
    style_header.fontSize = 10

    edad = Paragraph('''EDAD''', style_header)
    sexo = Paragraph('''SEXO''', style_header)
    suspect = Paragraph('''SOSPECHOSOS''', style_header)
    recovered = Paragraph('''RECUPERADOS''', style_header)
    confirmed = Paragraph('''CONFIRMADOS''', style_header)
    fallecidos = Paragraph('''FALLECIDOS''', style_header)

    table_data = [[edad, sexo, suspect, recovered, confirmed, fallecidos]]

    style_data = styles['BodyText']
    style_data.alignment = TA_CENTER
    style_data.fontSize = 10

    high = 650

    # traemos los pacientes en el rango de fechas
    start = datetime.datetime(int(anio1), int(mes1), int(dia1))
    end = datetime.datetime(int(anio2), int(mes2), int(dia2))

    patients = Patient.objects.filter(date_register__range=[start, end])

    age_ranges = [("<18", (0, 17)), ("18 - 65", (18, 65)), ("> 65", (65, 100))]

    for age, agerange in age_ranges:
        for sex in ["M", "F"]:
            patients_filtered = patients.filter(age__range=(agerange), sex__iexact=sex)

            num_suspect = patients_filtered.filter(state__startswith="S").count()
            num_recovered = patients_filtered.filter(state__startswith="C").count()
            num_confirmed = patients_filtered.filter(state__startswith="R").count()
            num_dead = patients_filtered.filter(severity__startswith="F").count()

            strings = [age, sex, num_suspect, num_recovered, num_confirmed, num_dead]
            table_data.append([Paragraph(str(string), style_data)
                               for string in strings])
            high -= 18

    width, height = A4
    table = Table(table_data, colWidths=[
        0.7 * inch, 0.6 * inch, 1.5 * inch, 1.5 * inch, 1.5 * inch, 1.4 * inch])
    table.setStyle(TableStyle([
        ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
        ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
    ]))

    table.wrapOn(c, width, height)
    table.drawOn(c, 30, high)
    c.showPage()

    # Guardamos todo
    c.save()
    pdf = buffer.getvalue()
    buffer.close()
    response.write(pdf)

    return response
