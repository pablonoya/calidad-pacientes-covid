import React from 'react';
import axios from 'axios';

import Patient from '../components/Patients';


export default class PatientsListSuspect extends React.Component{

    state={
        patientsSuspect:[]
    }

    componentDidMount(){
        //pacientes sospechosos
        axios.get('http://localhost:8000/api/patients/suspect/')
            .then(res=> {
                this.setState({
                    patientsSuspect:res.data.data
                });
                
            })

    }

    render(){
        return(
            <div>
            <h4>Pacientes Sospechosos</h4>   
            <Patient data={this.state.patientsSuspect}/>
            </div>
        )
    }
}