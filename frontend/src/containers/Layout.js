import React from 'react';
import {Link} from 'react-router-dom';

import { Layout, Menu,Breadcrumb,Input,Button, Modal,Collapse  } from 'antd';

import {
  SmileOutlined,
  BookOutlined,
  FrownOutlined,
  BugOutlined,
  FileAddOutlined,
  QuestionCircleOutlined,
  DownloadOutlined,
  UserOutlined,
  BarChartOutlined
} from '@ant-design/icons';

import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';
import './style.css';
const { Header, Content, Footer, Sider } = Layout;
const { SubMenu } = Menu;
const { Panel } = Collapse;
class CustonLayout extends React.Component{

  constructor(props) {
    super(props);
    // Don't call this.setState() here!
    this.state = {
      collapsed: false,
      nombre:'',
      visible1: false,
      visible2: false,
      estado:'3',
      date1: new Date(),
      date2: new Date(),
    };


  } 

      showModal1 = () => {
          this.setState({
          visible1: true,
        });
      };

      showModal2 = () => {
        this.setState({
        visible2: true,
      });
    };

      handleOk = e => {
        this.setState({
          visible1: false,
          visible2: false,
        });
      };
    
      handleCancel = e => {
        this.setState({
          visible1: false,
          visible2: false,
        });
      };

     onCollapse = collapsed => {
      console.log(collapsed);
      this.setState({ collapsed });
    };

    onChange1 = date1 => this.setState({ date1 })
    onChange2 = date2 => this.setState({ date2 })

    buscar=(p)=>{
      return '/nombre/'+p
    };

    llamado=(op,d1,m1,a1,d2,m2,a2)=>{
      switch(op){
        case "1":return "http://localhost:8000/pdf/recovered/"+d1+"/"+m1+"/"+a1+"/"+d2+"/"+m2+"/"+a2;break
        case "2":return "http://localhost:8000/pdf/confirmed/"+d1+"/"+m1+"/"+a1+"/"+d2+"/"+m2+"/"+a2;break
        case "3":return "http://localhost:8000/pdf/suspected/"+d1+"/"+m1+"/"+a1+"/"+d2+"/"+m2+"/"+a2;break
        case "4":return "http://localhost:8000/pdf/statistics/"+d1+"/"+m1+"/"+a1+"/"+d2+"/"+m2+"/"+a2;break
      }
      
    }

  render(){

    return (
      <Layout style={{ minHeight: '100vh' }}>
      <Sider collapsible collapsed={this.state.collapsed} onCollapse={this.onCollapse}>
        <div style={{    height: 32, background: 'rgba(255, 255, 255, 0.2)', margin: 16}}>
          {(this.state.collapsed)?
            <center>
              <p style={{color:'white', textAlign:'center',fontSize:20}}> <BugOutlined /></p>
            </center>
            :
          <div>
            <center>
              <a href="/">
              <p style={{color:'white', textAlign:'center',fontSize:20}}> <BugOutlined /> Covid-2019 </p>
              </a>
            </center>
          </div>
            }
        </div>
        <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline">
          <SubMenu key="sub1" icon={<UserOutlined />} title="Pacientes">
            <Menu.Item key="1" icon={<BookOutlined style={{fontSize:20}}/>}>
              <Link to="/">Todos</Link>
            </Menu.Item>

            <Menu.Item key="3" icon={<SmileOutlined style={{fontSize:20}}/>}>
              <Link to="/recovered">Recuperados</Link>
            </Menu.Item>

            <Menu.Item key="4" icon={<FrownOutlined style={{fontSize:20}}/>}>
             <Link to="/dead">Fallecidos</Link>
            </Menu.Item>

            <Menu.Item key="5" icon={<BugOutlined style={{fontSize:20}}/>}>
              <Link to="/confirmed">Confirmados</Link>
            </Menu.Item>

            <Menu.Item key="6" icon={<QuestionCircleOutlined style={{fontSize:20}}/>}>
              <Link to="/suspect">Sospechosos</Link>
            </Menu.Item>

            

          </SubMenu>
          

          <Menu.Item key="2" icon={<FileAddOutlined style={{fontSize:20}}/>}>
            <Link to="/add">Añadir</Link>
          </Menu.Item>
          
          <Menu.Item  icon={<DownloadOutlined style={{fontSize:20}}/>} onClick={this.showModal1}>
            Informes
          </Menu.Item>
          <Menu.Item key="7" icon={<BarChartOutlined style={{fontSize:20}}/>} onClick={this.showModal2}>
            estadisticas
          </Menu.Item>


        </Menu>
      </Sider>
      
      <Modal
          title= 'Reportes'
          visible={this.state.visible1}
          onOk={this.handleOk}
          cancelText='Cancelar'
          onCancel={this.handleCancel}
          width={1000}
          style={{ top: 20 }}
        >
          <center>
             <div style={{width:300,marginBottom:25}}> <p>Estado:</p>
                <select name="estado" className="form-control" onChange={(ev)=>this.setState({estado:ev.target.value})}>
                  <option value="3">Sospechoso</option>
                  <option value="2">Confirmado</option>
                  <option value="1">Recuperado</option>
                </select>
              </div>
          </center>
          <div style={{display:'flex',flexDirection:'row',width:900}} >
            <p style={{paddingLeft:15,paddingRight:15}}>De:</p>
            <Collapse accordion style={{width:'50%'}}>
            <Panel header= {'Fecha inicial :    '+this.state.date1.getDate()+'/'+(this.state.date1.getMonth()+1)+'/'+this.state.date1.getFullYear()} key="1" style={{alignItems:'center'}}>
            <Calendar
              onChange={this.onChange1}
              value={this.state.date1}
            />
            </Panel>      
          </Collapse>
          <p style={{paddingLeft:15,paddingRight:15}}>Hasta:</p>
          <Collapse accordion style={{width:'50%'}}>
          <Panel header= {'Fecha Final :    '+this.state.date2.getDate()+'/'+(this.state.date2.getMonth()+1)+'/'+this.state.date2.getFullYear()}  key="2">
            <Calendar
              onChange={this.onChange2}
              value={this.state.date1}
            />
            </Panel>      
          </Collapse>
          </div>
          <div style={{display:'flex',flexDirection:'row',justifyContent:'space-around',alignItems:'center',margin:15}}><a target="_blank" href={this.llamado(this.state.estado,this.state.date1.getDate(),this.state.date1.getMonth()+1,this.state.date1.getFullYear(),this.state.date2.getDate(),this.state.date2.getMonth()+1,this.state.date2.getFullYear())}><Button type="primary" icon={<DownloadOutlined />} size={25} > DESCARGAR</Button></a></div>

        </Modal>


        <Modal
          title= 'Estadisticas'
          visible={this.state.visible2}
          onOk={this.handleOk}
          cancelText='Cancelar'
          onCancel={this.handleCancel}
          width={1000}
          style={{ top: 20 }}
        >
          <center>
             <div style={{width:300,marginBottom:25}}> <p>Estadisticas:</p>
              </div>
          </center>
          <div style={{display:'flex',flexDirection:'row',width:900}} >
            <p style={{paddingLeft:15,paddingRight:15}}>De:</p>
            <Collapse accordion style={{width:'50%'}}>
            <Panel header= {'Fecha inicial :    '+this.state.date1.getDate()+'/'+(this.state.date1.getMonth()+1)+'/'+this.state.date1.getFullYear()} key="1" style={{alignItems:'center'}}>
            <Calendar
              onChange={this.onChange1}
              value={this.state.date1}
            />
            </Panel>      
          </Collapse>
          <p style={{paddingLeft:15,paddingRight:15}}>Hasta:</p>
          <Collapse accordion style={{width:'50%'}}>
          <Panel header= {'Fecha Final :    '+this.state.date2.getDate()+'/'+(this.state.date2.getMonth()+1)+'/'+this.state.date2.getFullYear()}  key="2">
            <Calendar
              onChange={this.onChange2}
              value={this.state.date1}
            />
            </Panel>      
          </Collapse>
          </div>
          <div style={{display:'flex',flexDirection:'row',justifyContent:'space-around',alignItems:'center',margin:15}}><a target="_blank" href={this.llamado("4",this.state.date1.getDate(),this.state.date1.getMonth()+1,this.state.date1.getFullYear(),this.state.date2.getDate(),this.state.date2.getMonth()+1,this.state.date2.getFullYear())}><Button type="primary" icon={<DownloadOutlined />} size={25} > GENERAR</Button></a></div>

        </Modal>


      <Layout className="site-layout">
        <Content style={{ margin: '10PX 10PX' }}>
          <Breadcrumb style={{ margin: '10px 10px',display:'flex',flexDirection:'row',justifyContent:'flex-end'}}>
 
            <Input style={{width:'30%'}} type="text" class="form-control" onChange={(ev)=>this.setState({nombre:ev.target.value})}/>
          <Link style={{color:'#fff',borderRadius:5,backgroundColor:'#259af6',padding:5,textAlign:'center',marginLeft:10}} to={(event)=>this.buscar(this.state.nombre)}>Buscar</Link>

          </Breadcrumb>
          <div className="site-layout-background" style={{ padding: 24, minHeight: 360 }}>
            {this.props.children}
          </div>
        </Content>
        <Footer>
            <small>Todos los derechos estan reservados &copy; 2020</small>
       </Footer>

      </Layout>
    </Layout>
  );

  }

}

export default CustonLayout

