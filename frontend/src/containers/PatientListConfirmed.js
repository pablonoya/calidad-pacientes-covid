import React from 'react';
import axios from 'axios';

import Patient from '../components/Patients';


export default class PatientsListConfirmed extends React.Component{

    state={
        patientsConfirmed:[]
    }

    componentDidMount(){
        //pacientes muertos
        axios.get('http://localhost:8000/api/patients/confirmed/')
            .then(res=> {
                this.setState({
                    patientsConfirmed:res.data.data
                });
                
            })

    }

    render(){
        return(
            <div>
            <h4>Pacientes confirmados</h4>   
            <Patient data={this.state.patientsConfirmed}/>
            </div>
        )
    }
}