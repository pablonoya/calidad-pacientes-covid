import React from 'react';
import axios from 'axios';

import Patient from '../components/Patients';

export default class PatientsList extends React.Component{

    state={
        patients:[]
    }

    componentDidMount(){
        //todos los pacientes
        axios.get('http://localhost:8000/api/patients')
            .then(res=> {
                this.setState({
                    patients:res.data.data
                });
            })

    }

    render(){
        return(
            <div>
             <h4>LISTA PACIENTES</h4>
            <Patient data={this.state.patients}/>
            </div>
        )
    }
}