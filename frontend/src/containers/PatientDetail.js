import React from 'react';
import axios from 'axios';

import {Card,Button} from 'antd'

export default class PatientDetail extends React.Component{

    state={
        patient:{}
    }

    handleSubmit = event =>{
        event.preventDefault();

        const patientUp = {
            first_name:this.state.patient.first_name,
            last_name:this.state.patient.last_name,
            age:this.state.patient.age,
            sex: this.state.patient.sex,
            base_diseases:this.state.patient.base_diseases,
            state:event.target.elements.estado.value,
            severity: this.state.patient.severity

        };
        

        const pk = this.props.match.params.pk;
        axios
            .put(`http://localhost:8000/api/patients/${pk}`,patientUp)
            .then(res=> {
                alert("Paciente Actualizado");
                this.componentDidMount();
                
            });
    };

    componentDidMount(){
        
        const pk = this.props.match.params.pk;
        axios.get(`http://localhost:8000/api/patients/${pk}`)
            .then(res=> {
                this.setState({
                    patient:res.data
                });

            })
        


    }

    render(){
        return(
            <Card className="border">
                <div className="card-header">
                    <h4>ACTUALIZAR ESTADO</h4>
                </div>
                <div className="card-body">
                <h5>DATOS ACTUALES</h5>
                <div><b>Apellido: </b>{this.state.patient.last_name}</div>
                <div><b>Edad: </b>{this.state.patient.age}</div>
                <div><b>Sexo: </b>{convertir(this.state.patient.sex)}</div>
                <div><b>Enfermedades base: </b>{this.state.patient.base_diseases}</div>
                <div><b>Estado: </b>{convertir(this.state.patient.state)}</div>
                <div><b>Gravedad: </b>{convertir(this.state.patient.severity)}</div>

                <h5 className="my-4">CAMBIAR ESTADO</h5>
                <form onSubmit={this.handleSubmit}>
                    <select name="estado" className="form-control" style={{ width: 160 }}>
                        <option value="S">Sospechoso</option>
                        <option value="C">Confirmado</option>
                        <option value="R">Recuperado</option>
                    </select>

                    <Button className="form-group my-4" type="submit" htmlType="submit" value="Registrar">Actualizar</Button>
                </form>
                </div>
            </Card>
        )
    }
}
function convertir(char){
    switch(char){
      case 'M': return 'Masculino';
      case 'F': return 'Femenino';
      
      case 'S': return 'Sospechoso';
      case 'C': return 'Confirmado';
      case 'R': return 'Recuperado';
  
      case 'LV': return 'Leve';
      case 'GV': return 'Grave';
      case 'MG': return 'Muy Grave';
      case 'FC': return 'Fallecido';
  
      default: return char;
    }
  }