import React from 'react';
import axios from 'axios';

import Patient from '../components/Patients';


export default class PatientsListRecovered extends React.Component{

    state={
        patientsRecovered:[]
    }

    componentDidMount(){
        //pacientes recuperados
        axios.get('http://localhost:8000/api/patients/recovered/')
        .then(res=> {
            this.setState({
                patientsRecovered:res.data.data
            });
            
        })

    }

    render(){
        return(
            <div>
            <h4>Pacientes Recuperados</h4>   
            <Patient data={this.state.patientsRecovered}/>
            </div>
        )
    }
}