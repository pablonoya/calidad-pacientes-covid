import React from 'react';
import axios from 'axios';

import Patient from '../components/Patients';


export default class ShearchPatientList extends React.Component{

    state={
        patientsSearch:[]
    }

    componentDidMount(){
        const first_name = this.props.match.params.first_name;
        axios.get(`http://localhost:8000/api/patients?name=${first_name}`)
            .then(res=> {
                this.setState({
                    patientsSearch:res.data.data
                });
            })
    }

    render(){
        return(
            <div>
            <h4>Resultado de Búsqueda</h4>   
            <Patient data={this.state.patientsSearch}/>
            </div>
        )
    }
}