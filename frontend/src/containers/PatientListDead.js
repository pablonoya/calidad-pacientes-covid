import React from 'react';
import axios from 'axios';

import Patient from '../components/Patients';


export default class PatientsListDead extends React.Component{

    state={
        patientsDead:[]
    }

    componentDidMount(){
        //pacientes muertos
        axios.get('http://localhost:8000/api/patients/dead')
            .then(res=> {
                this.setState({
                    patientsDead:res.data.data
                });
                
            })

    }

    render(){
        return(
            <div>
            <h4>Pacientes fallecidos</h4>   
            <Patient data={this.state.patientsDead}/>
            </div>
        )
    }
}