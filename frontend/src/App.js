import React, { Component } from "react";
import {BrowserRouter as Router} from 'react-router-dom';
import BaseRouter from './routes';

import './App.css';
import 'antd/dist/antd.css';


import CustonLayout from './containers/Layout';

class App extends Component {
  render() {
    return (
        <div className="App">
          <Router>
            <CustonLayout>
              <BaseRouter/>
            </CustonLayout>
          </Router>
        </div> 
    );
  }
}

export default App;