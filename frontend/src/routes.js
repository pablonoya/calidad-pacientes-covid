import React from 'react';
import {Route, Switch} from 'react-router-dom';

import PatientsList from './containers/PatientListView';
import PatientsListDead from './containers/PatientListDead';
import PatientsListRecovered from './containers/PatientListRecovered';
import PatientDetail from './containers/PatientDetail';
import PatientsListSuspect from './containers/PatientListSuspect';
import PatientsListConfirmed from './containers/PatientListConfirmed';
import ShearchPatientList from './containers/ShearchPatientList';
import Estadisticas from './containers/Estadisticas';


import Form from './components/Form';

const BaseRoutes = () =>(
    <div>
        <Switch>
            <Route exact path='/' component={PatientsList}/>
            <Route exact path='/add' component={Form}/>
            <Route exact path='/dead' component={PatientsListDead}/>
            <Route exact path='/recovered' component={PatientsListRecovered}/>
            <Route exact path='/confirmed' component={PatientsListConfirmed}/>
            <Route exact path='/estadisticas' component={Estadisticas}/>
            <Route exact path='/suspect' component={PatientsListSuspect}/> 
            <Route exact path='/nombre/:first_name' component={ShearchPatientList}/> 

           <Route exact path='/:pk' component={PatientDetail}/>
            
        </Switch>
    </div>
);

export default BaseRoutes;