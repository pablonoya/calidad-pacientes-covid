import React from 'react';
import { List, Card} from 'antd';

const gridStyle = {
  width: '50%',
  textAlign: 'center',
  padding:8,
  display:'flex',
  justifyContent:'space-around',
  
};

const Patient =(props)=>{

    return(
      <List
        itemLayout="vertical"
        size="large"
        pagination={{
          onChange: page => {
            console.log(page);
          },
          pageSize: 3,
        }}
        dataSource={props.data}

        renderItem={item => (
          <List.Item
            key={item.pk}
            style={{padding:0}}
          > 
          <Card title={<div style={{display:'flex',flexDirection:'row',justifyContent:'space-between'}}>
            <b><h6 style={{color:' #036992 '}}>{item.first_name+" "+item.last_name}</h6></b>
             <a href={`/${item.pk}`}>Actualizar estado</a></div>} >
            <Card.Grid style={gridStyle}><span><b style={{color:' #0a1a72'}}>Numero de paciente: </b>{item.pk}</span> <span><b style={{color:' #0a1a72'}}>Edad: </b>{item.age}</span> <span><b style={{color:' #0a1a72'}}>Sexo: </b>{convertir(item.sex)}</span> </Card.Grid>
            <Card.Grid style={gridStyle}><span><b style={{color:' #0a1a72'}}>Enfermedades base: </b>{item.base_diseases}</span> <span><b style={{color:' #0a1a72'}}>Estado: </b>{convertir(item.state)} </span><span><b style={{color:' #0a1a72'}}>Gravedad: </b>{convertir(item.severity)}</span></Card.Grid>
           
          </Card>

          </List.Item>
        )}
      />
    )
}



function convertir(char){
  switch(char){
    case 'M': return 'Masculino';
    case 'F': return 'Femenino';
    
    case 'S': return 'Sospechoso';
    case 'C': return 'Confirmado';
    case 'R': return 'Recuperado';

    case 'LV': return 'Leve';
    case 'GV': return 'Grave';
    case 'MG': return 'Muy Grave';
    case 'FC': return 'Fallecido';

    default: return char;
  }
}
export default Patient;