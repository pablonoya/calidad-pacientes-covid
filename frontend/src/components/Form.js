import React, { Component } from 'react';
import { Button} from 'antd';
import '../App.css';

import axios from 'axios';

class CustomForm extends React.Component{


    handleSubmit = event =>{
        event.preventDefault();

        const patient ={
            first_name:event.target.elements.nombre.value,
            last_name:event.target.elements.apellido.value,
            age:event.target.elements.edad.value,
            sex: event.target.elements.sexo.value,
            base_diseases:event.target.elements.enfBase.value,
            state:event.target.elements.estado.value,
            severity: event.target.elements.gravedad.value

        };
        console.log(patient);
        axios
            .post('http://localhost:8000/api/patients',patient)
            .then(res=> {
                alert("Paciente Agregado");
                console.log(res);
                console.log(res.data);
            });
    };


  render(){
    return(
      <div className="container">
        <h2>Agregar pacientes</h2>
        <form onSubmit={this.handleSubmit}> 
        <div className="form-group">
            <label>Nombre </label>
            <input name="nombre" type="text" class="form-control" ></input>
          </div>
          <div className="form-group">
            <label>Apellido</label>
            <input name="apellido" type="text" class="form-control" ></input>
          </div>
          <div style={{display:'flex',justifyContent:'space-between'}} className="form-group">
            <span><label>Edad</label>
            <input name="edad" type="number" class="form-control" ></input></span>
            <span><label>Sexo</label>
            <select name="sexo" className="form-control">
              <option value="F">Femenino</option>
              <option value="M">Masculino</option>
            </select></span>
            <span><label>Estado</label>
            <select name="estado" className="form-control">
              <option value="S">Sospechoso</option>
              <option value="C">Confirmado</option>
              <option value="R">Recuperado</option>
            </select></span>
            
            <span><label>Gravedad</label>
            <select name="gravedad" className="form-control" >
              <option value="LV">Leve</option>
              <option value="GV">Grave</option>
              <option value="MG">Muy Grave</option>
              <option value="FC">Fallecido</option>
            </select></span>

          </div>
          <div className="form-group">
            <label>Enfermedades Base</label>
            <textarea name="enfBase" class="form-control" rows="3"></textarea>
          </div>
          
          <center><Button style={{ backgroundColor:' #259af6 ',color:'#fff',borderRadius:5 }} className="form-group " type="submit" htmlType="submit" value="Registrar">Añadir</Button></center>
          
        </form>
      </div>
    )
  }
}

export default CustomForm;